import java.util.Scanner;

public class Shop{
	public static void main(String []args){
		
		Scanner obj = new Scanner(System.in);
		Clothes[] clothe = new Clothes[4];
			for(int i = 0; i < 4; i++){
				clothe[i]= new Clothes();
				System.out.println("What is the clothe size?");
				clothe[i].clothesSize = obj.nextInt();
				System.out.println("What is the price?");
				clothe[i].clothesPrice = obj.nextDouble();
				System.out.println("What is the colour?");
				clothe[i].clothesColour = obj.next();
			}
			    
				System.out.println("Last product size is: " + clothe[3].clothesSize);
				System.out.println("Last product price is: " + clothe[3].clothesPrice);
				System.out.println("Last product colour is: " + clothe[3].clothesColour);
				
				System.out.println("Instance method in part 1 is");
				clothe[3].instanceMethod1();
	}
}

